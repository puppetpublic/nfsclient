#
# This module gives nfsclient service
# TODO: include nfsnobody user?

class nfsclient (
  $nfsv4 = false,
) {

  include base::portmap

  case $::operatingsystem {
    'Debian': {
      package { 'nfs-common' :  ensure => present }
      service { 'nfs-common':
        ensure    => running,
        enable    => true,
        hasstatus => true,
        restart   => '/etc/init.d/nfs-common restart',
        require   => Package['nfs-common'],
      }
    }

    'Ubuntu': {
      package { 'nfs-common' :  ensure => present }

      # If using NFSv4, be sure to enable and start rpc.gssd
      if ($nfsv4) {
        package { 'nfs4-acl-tools':
          ensure => present,
        }
        file_line { 'nfsv4_gssd':
          path    => '/etc/default/nfs-common',
          line    => 'NEED_GSSD=yes',
          match   => '^NEED_GSSD=',
          require => Package['nfs-common'],
        }
        service { 'gssd':
          ensure  => running,
          enable  => true,
          require => [
                      File_Line['nfsv4_gssd'],
                      Package['nfs-common'],
                     ],
        }
      }
    }

    'RedHat': {
      package { 'nfs-utils': ensure => present }
      service {
        'netfs':
          enable    => true,
          hasstatus => true,
          require   => Package['nfs-utils'];
        'nfs':
          ensure    => running,
          enable    => true,
          hasstatus => true,
          require   => Package['nfs-utils'];
        'nfslock':
          ensure    => running,
          enable    => true,
          hasstatus => true,
          require   => Package['nfs-utils'];
      }
    }

    'CentOS': {
      package { 'nfs-utils': ensure => present }
      service { 'nfs':
        ensure    => running,
        hasstatus => true,
        require   => Package['nfs-utils'],
      }
      # CentOS7 does not have either of these services; if we get
      # hands on a genuine RHEL7 host, should test there too
      if ($::lsbmajdistrelease != '7') {
        service {
          'netfs':
            enable    => true,
            hasstatus => true,
            require   => Package['nfs-utils'];
          'nfslock':
            ensure    => running,
            enable    => true,
            hasstatus => true,
            require   => Package['nfs-utils'];
        }
      }
    }

    default: {
      fail ("Unknown OS '$::operatingsystem'")
    }
  }
}
