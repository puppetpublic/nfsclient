# Mount a NFS share
define nfsclient::nfsmount(
  $device,
  $ensure = 'mounted',
  $options = 'defaults,nosuid',
) {
  case $ensure {
    present: {
      # add path to fstab. Do not mount it or unmount it
      # if it is mounted.
    }
    absent: {
      # remove path from fstab and unmount it
    }
    mounted: {
      # add to fstab and mount it
    }
    default: { crit "Invalid ensure value: $ensure" }
  }

  # Add "_netdev" to the options string
  $options_string = "${options},_netdev"

  # Do the mount!
  if (!$::packer_build_name) {
    mount { $name:
      ensure   => $ensure,
      atboot   => yes,
      fstype   => nfs,
      pass     => 0,
      dump     => 0,
      remounts => true,
      device   => $device,
      options  => $options_string,
      require  => Class['nfsclient'],
    }
  }
}
